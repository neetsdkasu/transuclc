﻿namespace TransUcLc
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPaste = new System.Windows.Forms.Button();
            this.buttonTrans = new System.Windows.Forms.Button();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.panelUcLc = new System.Windows.Forms.Panel();
            this.radioLc = new System.Windows.Forms.RadioButton();
            this.radioUc = new System.Windows.Forms.RadioButton();
            this.panelRange = new System.Windows.Forms.Panel();
            this.radioSelection = new System.Windows.Forms.RadioButton();
            this.radioAll = new System.Windows.Forms.RadioButton();
            this.textBox = new System.Windows.Forms.TextBox();
            this.panelUcLc.SuspendLayout();
            this.panelRange.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonPaste
            // 
            this.buttonPaste.Location = new System.Drawing.Point(12, 12);
            this.buttonPaste.Name = "buttonPaste";
            this.buttonPaste.Size = new System.Drawing.Size(91, 33);
            this.buttonPaste.TabIndex = 0;
            this.buttonPaste.Text = "&Paste";
            this.buttonPaste.UseVisualStyleBackColor = true;
            this.buttonPaste.Click += new System.EventHandler(this.buttonPaste_Click);
            // 
            // buttonTrans
            // 
            this.buttonTrans.Location = new System.Drawing.Point(109, 12);
            this.buttonTrans.Name = "buttonTrans";
            this.buttonTrans.Size = new System.Drawing.Size(91, 33);
            this.buttonTrans.TabIndex = 1;
            this.buttonTrans.Text = "&Trans";
            this.buttonTrans.UseVisualStyleBackColor = true;
            this.buttonTrans.Click += new System.EventHandler(this.buttonTrans_Click);
            // 
            // buttonCopy
            // 
            this.buttonCopy.Location = new System.Drawing.Point(397, 12);
            this.buttonCopy.Name = "buttonCopy";
            this.buttonCopy.Size = new System.Drawing.Size(91, 33);
            this.buttonCopy.TabIndex = 2;
            this.buttonCopy.Text = "&Copy";
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // panelUcLc
            // 
            this.panelUcLc.Controls.Add(this.radioLc);
            this.panelUcLc.Controls.Add(this.radioUc);
            this.panelUcLc.Location = new System.Drawing.Point(206, 12);
            this.panelUcLc.Name = "panelUcLc";
            this.panelUcLc.Size = new System.Drawing.Size(97, 33);
            this.panelUcLc.TabIndex = 5;
            // 
            // radioLc
            // 
            this.radioLc.AutoSize = true;
            this.radioLc.Location = new System.Drawing.Point(0, 17);
            this.radioLc.Name = "radioLc";
            this.radioLc.Size = new System.Drawing.Size(93, 16);
            this.radioLc.TabIndex = 6;
            this.radioLc.Text = "to &LowerCase";
            this.radioLc.UseVisualStyleBackColor = true;
            // 
            // radioUc
            // 
            this.radioUc.AutoSize = true;
            this.radioUc.Checked = true;
            this.radioUc.Location = new System.Drawing.Point(0, 0);
            this.radioUc.Name = "radioUc";
            this.radioUc.Size = new System.Drawing.Size(93, 16);
            this.radioUc.TabIndex = 5;
            this.radioUc.TabStop = true;
            this.radioUc.Text = "to &UpperCase";
            this.radioUc.UseVisualStyleBackColor = true;
            // 
            // panelRange
            // 
            this.panelRange.Controls.Add(this.radioSelection);
            this.panelRange.Controls.Add(this.radioAll);
            this.panelRange.Location = new System.Drawing.Point(309, 12);
            this.panelRange.Name = "panelRange";
            this.panelRange.Size = new System.Drawing.Size(82, 33);
            this.panelRange.TabIndex = 6;
            // 
            // radioSelection
            // 
            this.radioSelection.AutoSize = true;
            this.radioSelection.Location = new System.Drawing.Point(0, 17);
            this.radioSelection.Name = "radioSelection";
            this.radioSelection.Size = new System.Drawing.Size(70, 16);
            this.radioSelection.TabIndex = 1;
            this.radioSelection.Text = "&Selection";
            this.radioSelection.UseVisualStyleBackColor = true;
            // 
            // radioAll
            // 
            this.radioAll.AutoSize = true;
            this.radioAll.Checked = true;
            this.radioAll.Location = new System.Drawing.Point(0, 0);
            this.radioAll.Name = "radioAll";
            this.radioAll.Size = new System.Drawing.Size(37, 16);
            this.radioAll.TabIndex = 0;
            this.radioAll.TabStop = true;
            this.radioAll.Text = "&All";
            this.radioAll.UseVisualStyleBackColor = true;
            // 
            // textBox
            // 
            this.textBox.AcceptsTab = true;
            this.textBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox.HideSelection = false;
            this.textBox.Location = new System.Drawing.Point(12, 51);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox.Size = new System.Drawing.Size(476, 229);
            this.textBox.TabIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 292);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.panelRange);
            this.Controls.Add(this.panelUcLc);
            this.Controls.Add(this.buttonCopy);
            this.Controls.Add(this.buttonTrans);
            this.Controls.Add(this.buttonPaste);
            this.Name = "MainForm";
            this.Text = "TransUcLc";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelUcLc.ResumeLayout(false);
            this.panelUcLc.PerformLayout();
            this.panelRange.ResumeLayout(false);
            this.panelRange.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPaste;
        private System.Windows.Forms.Button buttonTrans;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.Panel panelUcLc;
        private System.Windows.Forms.RadioButton radioLc;
        private System.Windows.Forms.RadioButton radioUc;
        private System.Windows.Forms.Panel panelRange;
        private System.Windows.Forms.RadioButton radioSelection;
        private System.Windows.Forms.RadioButton radioAll;
        private System.Windows.Forms.TextBox textBox;
    }
}


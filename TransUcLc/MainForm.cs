﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TransUcLc
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsText())
            {
                if (radioAll.Checked)
                {
                    textBox.Text = Clipboard.GetText();
                }
                else if (radioSelection.Checked)
                {
                    textBox.SelectedText = Clipboard.GetText();
                }
            }
        }

        private void buttonTrans_Click(object sender, EventArgs e)
        {
            string text = null;

            if (radioAll.Checked)
            {
                text = textBox.Text;
            }
            else if (radioSelection.Checked)
            {
                text = textBox.SelectedText;
            }

            if (text == null)
            {
                return;
            }

            if (radioUc.Checked)
            {
                text = text.ToUpper();
            }
            else if (radioLc.Checked)
            {
                text = text.ToLower();
            }

            if (radioAll.Checked)
            {
                textBox.Text = text;
            }
            else if (radioSelection.Checked)
            {
                var start = textBox.SelectionStart;
                textBox.SelectedText = text;
                textBox.SelectionStart = start;
                textBox.SelectionLength = text.Length;
            }
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            string text = null;

            if (radioAll.Checked)
            {
                text = textBox.Text;
            }
            else if (radioSelection.Checked)
            {
                text = textBox.SelectedText;                
            }

            if (text == null || text.Length == 0)
            {
                return;
            }

            Clipboard.Clear();
            Clipboard.SetText(text);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }
    }
}
